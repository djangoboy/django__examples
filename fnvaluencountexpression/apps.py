from django.apps import AppConfig


class FnvaluencountexpressionConfig(AppConfig):
    name = 'fnvaluencountexpression'
