from django.shortcuts import render
from .models import Book,Pizza
# Create your views here.
def BooksList(request):
    # books = Book.objects.all() >> book.author*.* == Book.objects.select_related('author').all()
    # books = Book.objects.select_related('author').all()
    books = {}
    pizzas = Pizza.objects.all().prefetch_related('toppings')
    # pizzas = Pizza.objects.all()
    return render(request,template_name='books/bookslist.html',context={'books':books,'pizzas':pizzas})



def staticTests(request):
    return render(request,template_name='books/statictests.html')



