from django.db import models

# Create your models here.


# models.py:

class TestManager(models.Manager):
    def test(self):
        print(dir(self))
        #here we can return anything what we want onthe call of this manager anywhere.
        return self.all()



class Author(models.Model):
    name = models.CharField(max_length=100)
    objects = TestManager()

class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey(Author,related_name='books',on_delete=models.CASCADE)
    # author = models.ForeignKey(Author, related_name='books')

class Topping(models.Model):
    name = models.CharField(max_length=30)

class Pizza(models.Model):
    name = models.CharField(max_length=50)
    toppings = models.ManyToManyField(Topping)

